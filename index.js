const Discord = require("discord.js");
const config = require("./config.json");
const fetch = require("node-fetch");
const moment = require("moment");

const client = new Discord.Client();
const baseUrl = config.BASE_URL;
const prefix = "~";

var shopCount = 0;
var mainMsg = new Discord.Message({ client: client });
var commandMsg = new Discord.Message({ client: client });

client.on("message", async (message) => {
  if (message.author.bot || !message.content.startsWith(prefix)) return;

  const commandBody = message.content.slice(prefix.length);
  var args = commandBody.split(" ");
  const command = args.shift().toLowerCase();

  switch (message.channel.name.toLowerCase()) {
    case "commands":
      switch (command) {
        case "claim":
          claimMember(message);
          break;

        case "create_member":
          args = args.join(" ").split("|");
          var name = args.shift().trim();
          var id = args.shift().trim();
          create(message, name, id);
          break;

        case "hours":
          if (args.length > 0) {
            hoursByName(message, args.join(" "));
          } else {
            hoursByUser(message);
          }
          break;

        case "checkin":
          checkIn(message, args.join(" "));
          break;

        case "checkout":
          checkOut(message, args.join(" "));
          break;

        default:
          if (command.includes("chrck") || command.includes("chck") || command.includes("chack") || command.includes("chwck") || command.includes("chdck") || command.includes("chsck") || command.includes("chfck"))
            message.channel.send("Matt I believe the command should be: " + (command.includes("out") ? "~checkout" : "~checkin") );
          else
            message.channel.send("I did not recognize that command");
          break;
      }
      break;

    case "shop-count":
      switch (command) {
        case "shop":
          shop();
          message.delete({ timeout: 5000 });
          break;

        default:
          message.channel
            .send("I did not recognize that command")
            .then((msg) => msg.delete({ timeout: 5000 }));
          message.delete({ timeout: 5000 });
          break;
      }

      break;

    default:
      break;
  }
});

const create = async (message, name, id) => {
  const url = baseUrl + `/users/createUser?id=${id}`;
  const body = {
    name: name,
    discord: message.author.id,
  };
  const response = await fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(body),
  });

  if (response.status != 201) {
    message.channel
      .send("There was an issue on the server, please try again soon.")
      .then((msg) => msg.delete({ timeout: 10000 }));
    return;
  }

  const data = await response.json();
  message.channel.send(`${data.data}`);
};

const hoursByName = async (message, name) => {
  const url = baseUrl + `/users/getUserByName?name=${name}`;
  const response = await fetch(url);

  if (response.status === 200) {
    const data = await response.json();
    var output = `**${
      data.data.name
    }:**\nHours committed: ${data.data.hours.toFixed(2)}`;

    message.channel.send(output);
  }

  // message.delete({ timeout: 60000 });
};

const hoursByUser = async (message) => {
  const url = baseUrl + `/users/getUserByDiscordId?id=${message.author.id}`;
  const response = await fetch(url);
  if (response.status === 200) {
    const data = await response.json();
    var output = `**${
      data.data.name
    }:**\nHours committed: ${data.data.hours.toFixed(2)}`;
    message.channel.send(output);
  }
};

const shop = async () => {
  const userString = await getShopCount();

  mainMsg.edit(
    `**There ${
      shopCount == 1 ? `is ${shopCount} person` : `are ${shopCount} people`
    } in the shop right now.**\n\nIn the shop right now:${userString}`
  );
};

const checkIn = async (message) => {
  const id = await getIdFromDiscord(message.author.id);
  const url = baseUrl + `/shop/checkIn?id=${id}`;

  const response = await fetch(url, {
    method: "POST",
  });

  if (response.status === 200 || response.status === 201) {
    message.reply("Check in successful.");
    // .then((msg) => msg.delete({ timeout: 10000 }));
  } else {
    message.reply("Check in was not successful. Please try again later");
    // .then((msg) => msg.delete({ timeout: 10000 }));
  }

  // message.delete({ timeout: 30000 });
};

const checkOut = async (message) => {
  const id = await getIdFromDiscord(message.author.id);
  const url = baseUrl + `/shop/checkOut?id=${id}`;

  const response = await fetch(url, {
    method: "POST",
  });

  if (response.status === 200 || response.status === 201) {
    message.reply("Check out successful.");
    // .then((msg) => msg.delete({ timeout: 10000 }));
  } else {
    message.reply("Check out was not successful. Please try again later");
    // .then((msg) => msg.delete({ timeout: 10000 }));
  }

  // message.delete({ timeout: 30000 });
};

const getIdFromDiscord = async (id) => {
  const url = baseUrl + `/users/getUserByDiscordId?id=${id}`;
  const response = await fetch(url);

  if (response.status === 200 || response.status === 201) {
    const data = await response.json();
    return data.id;
  } else {
    return 0;
  }
};

const getShopCount = async () => {
  const url = baseUrl + `/shop/getMembers`;

  const request = await fetch(url);
  const response = await request.json();

  shopCount = response.length;
  var userString = "";

  response.forEach((user) => {
    checkMissedClockOut(user);
    userString += `\n> ${user.name}`;
  });
  return userString;
};

const checkMissedClockOut = (user) => {
  var lastEnteredMoment = moment(user.lastEnteredShop);
  var lastEntered = lastEnteredMoment.startOf("hour").fromNow();
  var lastEnteredAmount = parseInt(
    lastEntered.slice(0, lastEntered.indexOf(" "))
  );
  if (lastEntered.includes("hour") && lastEnteredAmount > 2) {
    commandMsg.channel.send(
      `<@${user.discord}> It looks like you have been checked in for more than 2 hours. Maybe you should clock out and take a break?`
    ).then((msg) => msg.delete({ timeout: 30000 }));
  } else if (lastEntered.includes("day")) {
    commandMsg.channel.send(
      `<@${user.discord}> you have been clocked in for more than a day. Please clock out and inform the Shop Keeper what time you left.`
    ).then((msg) => msg.delete({ timeout: 30000 }));
  }
};

const claimMember = async (message) => {
  const url = baseUrl + "/users/getUsers";
  const updateUserUrl = baseUrl + "/users/updateUser?id=";

  const request = await fetch(url);
  const response = await request.json();

  var unclaimed = [];
  var userString = "";

  response.forEach((user) => {
    if (user.data.discord == "") {
      unclaimed.push(user);
      userString += `\n\`\`\`Name: ${user.data.name}\nNumber: ${user.id}\`\`\``;
    }
  });

  const filter = (m) => m.author.id === message.author.id;
  message.channel
    .send(
      `Please respond with the member number you want to claim.\n> _Expires in 20 Seconds, Type \`cancel\` to cancel._${userString}`
    )
    //.then(msg => msg.delete({ timeout: 20000 }))
    .catch((err) => {});

  message.channel
    .awaitMessages(filter, {
      max: 1, // do NOT change this
      time: 20000,
      errors: ["time"],
    })
    .then(async (collected) => {
      if (collected.first().content.toLowerCase() == "cancel") {
        message.channel.send("The command has been cancelled."); //.then(msg => msg.delete({ timeout: 10000 }));
      } else if (userString.includes(collected.first().content)) {
        await fetch(updateUserUrl + collected.first().content, {
          method: "PUT",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify({ discord: message.author.id }),
        })
          .then((response) => {
            message.channel.send("Successfully Claimed"); //.then(msg => msg.delete({ timeout: 10000 }));
          })
          .catch((error) => message.channel.send("Claim Failed")); //.then(msg => msg.delete({ timeout: 10000 })));
      } else {
        message.channel.send("Incorrect Number Entered"); //.then(msg => msg.delete({ timeout: 10000 }));
      }

      // collected.first().delete({timeout: 10000});
    })
    .catch((error) => {
      message.channel.send("Claiming timed out."); //.then(msg => msg.delete({ timeout: 10000 }));
    });
};

client.on("ready", async () => {
  console.log("Connecting...");

  client.user.setStatus("available"); // Can be 'available', 'idle', 'dnd', or 'invisible'
  client.user.setUsername("Watcher");
  client.user.setPresence({
    activity: { name: "Avoiding Covid" },
  });

  mainMsg.channel = client.channels.cache.find(
    (i) =>
      i.name.toLowerCase() == "shop-count"
  );

  commandMsg.channel = client.channels.cache.find(
    (i) =>
      i.name.toLowerCase() == "commands" &&
      i.parent.name.toLowerCase() == "utilities"
  );

  await getShopCount().then(async (userString) => {
    mainMsg = await mainMsg.channel.send(
      `**There ${
        shopCount == 1 ? `is ${shopCount} person` : `are ${shopCount} people`
      } in the shop right now.**\n\nIn the shop right now:${userString}`
    );
  });
});

client.setInterval(shop, 600000); // acts every 10 minutes

client.login(config.BOT_TOKEN);
